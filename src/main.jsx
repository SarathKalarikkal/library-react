import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css'
import Root from './routes/root';
import ErrorPage from './error-page';
import Books from './routes/books';
import Authors from './routes/authors';





const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Home />,
        errorElement: <ErrorPage />,

      },
      {
        path: "/books",
        element: <Books />,
        errorElement: <ErrorPage />
      },
      {
        path: "/books/book/:id",
        element: <Book />,
        errorElement: <ErrorPage />,
      },
      {
        path: "/authors",
        element: <Authors />,
        errorElement: <ErrorPage />
      },
      {
        path: "/authors/author/:id",
        element: <Author />,
        errorElement: <ErrorPage />,
      },
    ]
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>,
)
